import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CareerSearchPanelComponent } from './components/career-search-panel/career-search-panel.component';
import { CareerResultShowComponent } from './components/career-result-show/career-result-show.component';
import { PaginationComponent } from './components/pagination/pagination.component';


const appRoutes:Routes=[
  {path:'searchCareer/:careerclassification/:city/:keywords/:jobtype/:compensationtype/:employertype/:locationtype', component:CareerSearchPanelComponent},
  {path:'search',component:CareerSearchPanelComponent},
  {path:'showDetail/:id', component:CareerResultShowComponent},
  {path:'pagination',component:PaginationComponent},
  {path:'', redirectTo:'search',pathMatch:'full'},
  {path:'**',redirectTo:'search',pathMatch:'full'},
]

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forRoot(
      appRoutes,
      {enableTracing:false}
    )
  ],
  exports: [
    RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
