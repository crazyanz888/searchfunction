import {City,Compensation,JobType,EmployerType,LocationType,ParamOptions} from './career';

export const careerData : Object={
    Test:[
      'IT',
      'Education',
      'Government'
    ],
    Accounting:[
      'Acconts Offices/Clerks',
      'Analysis & Reporting',
      'Business Services & Corporate advisory'
    ],
    'Banking & Financial Services':[
      'Banking - Business',
      'Financial Planning'
    ],
    Education: [
      'Teaching - Primary School',
      'Teaching - Secondary School',
      'Teaching - High School',
      'Teaching-  Vocational',
      'Childcare & Outside School Hours Care'
    ],
    'Designing & Architecture':[
        'Graphic Design',
        'Web Design',
        'Urban Design',
    ],
  };

export const compensation: Compensation[]=[
  {compensationtype:'higher than 50K'},
  {compensationtype:'lower than 50K'},
]

export const city:City[]=[
    {cityID:1,cityName:'Auckland'},
    {cityID:2,cityName:'Christchurch'},
    {cityID:3,cityName:'Wellington'},
    {cityID:4,cityName:'Manukau City'},
    {cityID:5,cityName:'Waitakere'},
    {cityID:6,cityName:'North Shore'},
    {cityID:7,cityName:'Hamilton'},
    {cityID:8,cityName:'Dunedin'},
    {cityID:9,cityName:'Tauranga'},
    {cityID:10,cityName:'Lower Hutt'}
]
export const jobtype:JobType[]=[
  {jobtype:'Full time'},
  {jobtype:'Part time'},
  {jobtype:'Casual'},
]
export const employertype:EmployerType[]=[
  {employertype:'Employer Type 1'},
  {employertype:'Employer Type 2'},
  {employertype:'Employer Type 3'},
]
export const locationtype:LocationType[]=[
  {locationtype:'Location Type 1'},
  {locationtype:'Location Type 2'},
  {locationtype:'Location Type 3'},
]
