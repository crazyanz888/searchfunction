import {MatButtonModule,MatCheckboxModule,MatInputModule,MatIconModule} from '@angular/material';
import {NgModule} from '@angular/core';
import {MatSelectModule} from '@angular/material/select';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTreeModule} from '@angular/material/tree';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatMenuModule} from '@angular/material/menu';
import {MatDividerModule} from '@angular/material/divider';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
@NgModule({
    imports: [
        MatButtonModule,
        MatCheckboxModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatIconModule,
        MatToolbarModule,
        MatTreeModule,
        MatGridListModule,
        MatMenuModule,
        MatDividerModule,
        MatButtonToggleModule
    ],
    exports:[
        MatButtonModule,
        MatCheckboxModule,
        MatSelectModule,
        MatFormFieldModule,
        MatInputModule,
        MatPaginatorModule,
        MatIconModule,
        MatToolbarModule,
        MatTreeModule,
        MatGridListModule,
        MatMenuModule,
        MatDividerModule,
        MatButtonToggleModule
    ]
})
export class MaterialModule{}
