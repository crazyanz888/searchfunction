import { Injectable, Output,EventEmitter } from '@angular/core';
import {
  debounceTime, distinctUntilChanged, switchMap, catchError,map,tap
} from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class CareerService {
  paramOptions:any[]=[];
  careerResults:any;//overall variable
  apiUrl = 'http://192.168.1.72:8081/findjobs';
  // url = `${this.apiUrl}/${this.id}`;
  map=new Map();
  @Output() emitcareerResults:EventEmitter<Object>=new EventEmitter();
  @Output() emitcareerDetail:EventEmitter<Object>=new EventEmitter();
  @Output() emitClassification:EventEmitter<any>=new EventEmitter();
 constructor(
    // private careerService:CareerService,
    private http:HttpClient
    ) { }
    
 //get all the career list
 //paramOptions
 //if there is city in paramOptions, then emitLocation here.
 getCareers(params){
    return this.http.get(this.apiUrl);
  }
 getPaginatingCareers(params,page){
  return this.http.get(this.apiUrl+'?page='+page);
 }

  
emitCareer(params){
  this.getCareers(params).subscribe(
    res=>{
      this.emitcareerResults.emit(res);
      // console.log(res);
      console.log("emit successfully.")
    },
    err=>{return},
  )
}

  //try to get a certain career detail by id
  showCareerDetail(id){
    return this.http.get(this.apiUrl+ '/'+id)
  }
  


  //get classification and emit from mat-tree to search bar; 
  getClassification(data:any){
    this.emitClassification.emit(data);
  }
}
