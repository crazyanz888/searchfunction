import { Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import {CareerService} from '../../career.service';
import { Router } from "@angular/router"
import {
  debounceTime, distinctUntilChanged, switchMap
} from 'rxjs/operators';
import{ActivatedRoute,ParamMap} from '@angular/router';

@Component({
  selector: 'app-career-search-results',
  templateUrl: './career-search-results.component.html',
  styleUrls: ['./career-search-results.component.css']
})
export class CareerSearchResultsComponent implements OnInit {
  careersList:any;
  errorMessage:string;

  selectedCity:any;
  keyWords:any;
  jobType:any;
  employerType:any;
  ClassificationName:any;
  compensationType:any;
  locationType:any;
  paramOptions:any;
  job_type:any;
//for pagination
  currentPage = 0;
  pageNumber = 0;
  pages=[];
  totalPosts = 0;
  perPage = 0;
  errorFlag:boolean;
  errorInfo:string;
  loading: boolean;
  total:number;
  p:number=1;


  constructor(
    private careerService:CareerService,
    private route:ActivatedRoute,
    // private router:Router,
    ) { }

  ngOnInit() : void{
    this.getPage(1); 
    //in default situation, we can get the first page of the return data.
    this.defaultResults();
    
       this.careerService.emitcareerResults.subscribe(
         res=>{
           this.careersList = res['data'];
           this.careersList.map(cv=>
            {
              switch(cv.job_type){
                case 1:
                cv.job_name='full time';
                break;
                case 2:
                cv.job_name='part time';
                break;
                case 3:
                cv.job_name='casual';
                break;
              }})
            },
         err=>this.errorMessage="Oops"
       )
    
  }



  defaultResults(){
    this.route.queryParams.subscribe(queryParams=>
      {
        
          let map=new Map();
          this.selectedCity=queryParams.city;
          map.set("location",this.selectedCity);
          this.keyWords=queryParams.keywords;
          map.set("keywords",this.keyWords);
          queryParams.careerclassification == null?this.ClassificationName="":this.ClassificationName=queryParams.careerclassification;
          map.set("classification",this.ClassificationName);
          this.jobType=queryParams.jobtype;
          map.set("jobtype",this.jobType);
          // console.log(typeof(this.jobType));
          this.employerType=queryParams.employertype;
          map.set("employertype",this.employerType);
          this.locationType = queryParams.locationtype;
          map.set("locationtype",this.locationType);
          this.compensationType= queryParams.compensationtype;
          map.set("compensationtype",this.compensationType);
          map.forEach((value,key)=>console.log(key+":"+value));
          this.paramOptions = Array.from(map.entries()).reduce((main, [key, value]) => ({...main, [key]: value}), {});
          // console.log(this.paramOptions);
        }  );
      //judge parameters and get the first page of return data
      this.careerService.getCareers(this.paramOptions).subscribe(
        res=>{
          this.careersList = res['data'];
          // console.log(this.careersList);
          this.careersList.map(cv=>
            {
              switch(cv.job_type){
                case 1:
                cv.job_name='full time';
                break;
                case 2:
                cv.job_name='part time';
                break;
                case 3:
                cv.job_name='casual';
                break;
              }
            })
            console.log(this.careersList);
        },
        err=>this.errorMessage="Oops",
      )
  }

  getPage(page: number) {
    this.loading = true;
    //try to get data of this page
    this.careerService.getPaginatingCareers(this.paramOptions,page).subscribe(
      (res)=>{
        console.log(res);
        if(this.currentPage == 0){
          this.currentPage = res['current_page'];
          this.totalPosts = res['total'];
          this.perPage = res['per_page'];
          this.pageNumber = Math.ceil(this.totalPosts/this.perPage);
          // this.pageNumber=res['last_page'];
          if (this.pageNumber > 1){
            for (let i=1; i<this.pageNumber+1; i++){
              this.pages.push(i);
            }
            // console.log(this.pages);
          }else{
            this.pages.push(1);
          }
        }else{
          this.currentPage = res['current_page'];
        }

        this.careersList = res['data'];
        
        this.total=res['total'];
        this.p=page;
        this.loading = false;
      },
      (err)=>{
        console.warn(err)
        this.errorFlag = true;
        this.errorInfo = err;
      });
  }

  gotodetails(id) {
    //get detail, pass the id
    this.careerService.showCareerDetail(id);
  }

}
