import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerSearchResultsComponent } from './career-search-results.component';

describe('CareerSearchResultsComponent', () => {
  let component: CareerSearchResultsComponent;
  let fixture: ComponentFixture<CareerSearchResultsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerSearchResultsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerSearchResultsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
