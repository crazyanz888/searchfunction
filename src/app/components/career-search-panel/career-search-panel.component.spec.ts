import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerSearchPanelComponent } from './career-search-panel.component';

describe('CareerSearchPanelComponent', () => {
  let component: CareerSearchPanelComponent;
  let fixture: ComponentFixture<CareerSearchPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerSearchPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerSearchPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
