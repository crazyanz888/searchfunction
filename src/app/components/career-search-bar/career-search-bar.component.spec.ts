import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerSearchBarComponent } from './career-search-bar.component';

describe('CareerSearchBarComponent', () => {
  let component: CareerSearchBarComponent;
  let fixture: ComponentFixture<CareerSearchBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerSearchBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerSearchBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
