import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { city, compensation, jobtype, employertype, locationtype } from '../../careerData';
import { CareerService } from '../../career.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-career-search-bar',
  templateUrl: './career-search-bar.component.html',
  styleUrls: ['./career-search-bar.component.css']
})


export class CareerSearchBarComponent implements OnInit {

  cities = city;
  compensations = compensation;
  jobtypes = jobtype;
  employertypes = employertype;
  locationtypes = locationtype;

  displayMatTree = false;
  keyWords: any;
  selectedCity: any;
  errorMessage = "";
  ClassificationName = "";
  classficationArray: any[] = [];
  compensationType: any;
  locationType: any;
  jobType: any;
  employerType: any;
  paramOptions: any;

  constructor(
    private careerService: CareerService,
    private route: ActivatedRoute,
    private router: Router,
  ) { }

  ngOnInit() {
    this.getParams();
    //get classifcationArray
    this.careerService.emitClassification.subscribe(
      res => {
        this.ClassificationName = res.map(cv => cv.item)
      },
      err => { return },
    )
  }

  //search career and show them in the results page
  //pass parameters here! inclucing career classification, city, key words, sortof...
  searchCareer() {
    this.careerService.emitCareer(this.paramOptions)
  }

  clickExpandDropDown(){
    this.displayMatTree = true;
  }

  close(e){
    this.displayMatTree = false;
  }

  getParams() {
    this.route.queryParams.subscribe(queryParams => {

      let map = new Map();
      this.selectedCity = queryParams.city;
      map.set("location", this.selectedCity);
      this.keyWords = queryParams.keywords;
      map.set("keywords", this.keyWords);
      queryParams.careerclassification == null ?
         this.ClassificationName = "" :
           this.ClassificationName = queryParams.careerclassification;
      map.set("classification", this.ClassificationName);
      this.jobType = queryParams.jobtype;
      if (this.jobType == "Full time") 
        map.set("jobtype", 1);
      else if (this.jobType == "Part time")
       map.set("jobtype", 2);
      else if (this.jobType == "Casual")
       map.set("jobtype", 3);
      else 
        map.set("jobtype", 0);
      this.employerType = queryParams.employertype;
      map.set("employertype", this.employerType);
      this.locationType = queryParams.locationtype;
      map.set("locationtype", this.locationType);
      this.compensationType = queryParams.compensationtype;
      map.set("compensationtype", this.compensationType);
      // map.forEach((value,key)=>console.log(key+":"+value));
      this.paramOptions = Array.from(map.entries()).reduce((main, [key, value]) => ({ ...main, [key]: value }), {});
      console.log(this.paramOptions);
    })
  }
}
