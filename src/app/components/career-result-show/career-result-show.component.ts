import { Component, OnInit } from '@angular/core';
import {CareerService} from '../../career.service';
import {Router, ActivatedRoute, ParamMap} from '@angular/router';
import { $ } from 'protractor';

@Component({
  selector: 'app-career-result-show',
  templateUrl: './career-result-show.component.html',
  styleUrls: ['./career-result-show.component.css'],
})

export class CareerResultShowComponent implements OnInit {
  careerID:any;
  careerDetail:Object;
  errorMessage:string;
  job_type:string;
  job_id:number;
  job_description:any;
  job_requirement:any;
  constructor(
    private route:ActivatedRoute,
    private router:Router,
    private careerService:CareerService,
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params=>
      {this.job_id=params.id;
        this.careerService.showCareerDetail(this.job_id).subscribe(
          res=>{
            // console.log(res['data'].jobInfo);

            this.careerDetail=res['data'].jobInfo;
            this.job_description = this.careerDetail['job_description'];
            this.job_requirement = this.careerDetail['job_requirements']
            this.careerDetail['job_type']==1?this.job_type='full time':
            this.careerDetail['job_type']==2?this.job_type='part time':
              this.job_type='casual';
          },
          err=>this.errorMessage = "Oops",
        )
      })
  } 
  
  
  
}
