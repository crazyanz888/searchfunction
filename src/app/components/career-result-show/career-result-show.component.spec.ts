import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CareerResultShowComponent } from './career-result-show.component';

describe('CareerResultShowComponent', () => {
  let component: CareerResultShowComponent;
  let fixture: ComponentFixture<CareerResultShowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CareerResultShowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CareerResultShowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
