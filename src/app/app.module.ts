import { NgtUniversalModule } from '@ng-toolkit/universal';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material';
import { HttpModule } from '@angular/http';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { CareerSearchPanelComponent } from './components/career-search-panel/career-search-panel.component';
import { CareerSearchBarComponent } from './components/career-search-bar/career-search-bar.component';
import { CareerSearchResultsComponent } from './components/career-search-results/career-search-results.component';
import { CareerResultShowComponent } from './components/career-result-show/career-result-show.component';
import { MatTreeComponent } from './mat-tree/mat-tree.component';
import { NavbarComponent } from './supports/navbar/navbar.component';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { PaginationComponent } from './components/pagination/pagination.component';
import { ClickOutsideDirective } from './click-outside.directive';


@NgModule({
  declarations: [
    AppComponent,
    CareerSearchPanelComponent,
    CareerSearchBarComponent,
    CareerSearchResultsComponent,
    CareerResultShowComponent,
    MatTreeComponent,
    NavbarComponent,
    PaginationComponent,
    ClickOutsideDirective,
  ],
  imports:[
    CommonModule,
    NgtUniversalModule,
    CommonModule,    
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    HttpModule,
    HttpClientModule,
    ReactiveFormsModule,
    AppRoutingModule,
  ],
  schemas:[CUSTOM_ELEMENTS_SCHEMA],
  providers: [],
})
export class AppModule { }
