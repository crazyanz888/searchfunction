
export class City{
    cityID:number;
    cityName:string;
}
export class User {
    id: number;
    username: string;
    password: string;
    firstName: string;
    lastName: string;
  }
  export class Compensation{
      compensationtype:string;
  }
  export class JobType{
      jobtype:string;
  }
  export class EmployerType{
      employertype:string;
  }
  export class LocationType{
      locationtype:string;
  }
 export class ParamOptions{
    classification:any[];
    location:string;
    keywords:string[];
    compensationtype:string;
    jobtype:string;
    employertype:string;
    locationtype:string;
}
    